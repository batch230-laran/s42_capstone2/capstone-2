
 
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

 
 

// ------------------------- USER REGISTRATION


router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})




// ------------------------- USER AUTHENTICATION


router.post("/login", (request, response) => {
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})





// ------------------------- RETRIEVE USER DETAILS USING JWT


router.post("/details", auth.verify, userControllers.userDetails);





// ------------------------- CHECK-OUT (CREATE ORDER)


router.post("/checkout", auth.verify, userControllers.checkout);





module.exports = router;
