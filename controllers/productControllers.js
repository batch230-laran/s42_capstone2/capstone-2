

const mongoose = require("mongoose");
const Product = require("../models/Product.js");



// ------------------------- CREATE PRODUCT (ADMIN ONLY)


module.exports.addProduct = (reqBody, addedProduct) => {
	if(addedProduct.isAdmin == true) {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})

	return newProduct.save().then((newProduct, error) => {
		if(error) {
			return error;
		}
		else {
			return newProduct;
		}
	})
}
else {
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}




// ------------------------- RETRIEVE ALL ACTIVE PRODUCTS


module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}




// ------------------------- RETRIEVE SINGLE PRODUCT

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result =>{
		return result;
	})
}





// ------------------------- UPDATE PRODUCT INFORMATION (ADMIN ONLY)


module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				name: newData.product.name, 
				description: newData.product.description,
				price: newData.product.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return ("Product Information has been successfully updated")
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}




// ------------------------- ARCHIEVE PRODUCT (ADMIN ONLY)


module.exports.archiveProduct = (productId, archiveData) => {
	if(archiveData.isAdmin == true) {
		return Product.findByIdAndUpdate(productId, {
			isActive: archiveData.product.isActive,
		}).then((result, error) => {
			if(error) {
				return false;
			}
			else {
				return ("Product has been successfully archived")
			}
		})
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {
			return value});
	}
}






