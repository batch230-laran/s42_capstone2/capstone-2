


const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



 
// ------------------------- USER REGISTRATION
 

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
		})
	
	return newUser.save().then((user, error) => {
		if(error){
			return ("Duplicate user found")
		}
		else{
			return ("New user registered")
		}
	})
}





// ------------------------- USER AUTHENTICATION


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}
 




// ------------------------- RETRIEVE USER DETAILS USING JWT




module.exports.userDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	console.log(userData);


	return User.findById(userData.id).then(result => {
		result.password = "******";
		response.send(result);
		
	})
}





// ------------------------- CHECK OUT / CREATE ORDER


module.exports.checkout = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	let productName = await Product.findById(request.body.productId).then(result => result.name);

let newUserData = {
		userId: userData.id,
		email: userData.email,
		productId: request.body.productId,
		productName: productName,
		quantity: request.body.quantity
	}
	console.log(newUserData);


// ---------------- Update the user after creating an order

	let price = await Product.findById(newUserData.productId).then(result => result.price);

	let isUserUpdated = await User.findById(newUserData.userId)
	.then(user => {

/* 

{
		totalAmount: {type: Number},
		purchasedOn: {type: Date, default: new Date()},
		products: [
		{
			productId: {type: String, required: [true, "Product ID is required"]},
			productName: {type: String},
			quantity: {type: Number}
		}

*/

		user.orders.push({
			totalAmount: price,
			products: [
			{
				productId: newUserData.productId,
				productName: newUserData.productName,
				quantity: newUserData.quantity
			}]
			
		});

		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);


// ---------------- Update the product after buyer checkout

	let isProductUpdated = await Product.findById(newUserData.productId).then(product => {

		product.customers.push({
			userId: newUserData.userId,
			email: newUserData.email
		})

		product.stocks -= newUserData.quantity;

		return product.save()
		.then(result=>{
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isProductUpdated);

	(isUserUpdated == true &&  isProductUpdated == true)? response.send("Product Order Successful!") : response.send("There was a problem processing the order");

}







