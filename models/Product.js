

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {type: String, required: [true, "Product name is required"]},
	description: {type: String, required: [true, "Description is required"]},
	price: {type: Number, required: [true, "Price is required"]},
	stocks: Number,
	isActive: {type: Boolean, default: true},
	createdOn: {type: Date, default: new Date()},
	customers: [
	{
		orderId: {type: String},
		userId: {type: String, required: [true, "User ID is required"]},
		userEmail: {type: String},
		quantity: {type: Number},
		purchasedOn: {type: Date, default: new Date()}
	}]
})


module.exports = mongoose.model("Product", productSchema); 